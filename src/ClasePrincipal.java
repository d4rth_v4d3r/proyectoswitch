/**
 * 
 */

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
public class ClasePrincipal extends Compilador {

	public static void main(String[] args) throws Exception {
		Parser p = new Parser(ClasePrincipal.class.getClassLoader()
				.getResourceAsStream("resources/entrada.txt"),
				new ClasePrincipal());
		p.parse();
	}

	@Override
	public void reportarErrorLexico(String token, int linea, int columna) {
		// TODO Auto-generated method stub
		System.out.println(String.format("Token no reconocido: %s. (%d, %d)", token, linea, columna));
	}

	@Override
	public void reportarErrorSintactico(String mensaje, int linea, int columna) {
		// TODO Auto-generated method stub
		System.out.println(String.format("%s. (%d, %d)", mensaje, linea, columna));
	}

	@Override
	public void reportarErrorSemantico(String mensaje, int linea, int columna) {
		// TODO Auto-generated method stub

	}

	@Override
	public void write(String codigo) {
		// TODO Auto-generated method stub
		System.out.println(codigo);
	}

}
