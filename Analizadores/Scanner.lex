import java_cup.runtime.*;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
%%
%{
	private Compilador compi = null;

	Scanner(java.io.InputStream is, Compilador compi) {
		this(is);
		this.compi = compi;
	}

	private Symbol sym(int type) {
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value) {
		return new Symbol(type, yyline + 1, yycolumn + 1, value);
	}
%}

%init{
		
%init}

%eofval{	
		return sym(ParserSym.EOF);
%eofval}

%class Scanner
%public
%cup
%unicode
%ignorecase
%line  	//definimos el uso de contador de lineas
%char  	//definimos el uso de contador de caracteres
%column
%state COMMENT1, COMMENT2

character	= 	[A-Za-z]
digit		=	[0-9]
entero		=	{digit}+
id 			=	{character}({character}|{digit}|"_")*

%%

//=================================== INICIO DE EXPRESIONES REGULARES  =======================================

<YYINITIAL> {

	//=================================== INICIO KEYWORDS  =======================================

	"switch"				{ return sym(ParserSym.SWITCH); 	}
	"break"					{ return sym(ParserSym.BREAK); 	}
	"default"				{ return sym(ParserSym.DEFAULT); 	}
	"case"					{ return sym(ParserSym.CASE); 	}
	"{"						{ return sym(ParserSym.LLAVE_IZQ); 	}
	"}"						{ return sym(ParserSym.LLAVE_DER); 	}
	";"						{ return sym(ParserSym.PUNTO_COMA); 	}
	"("						{ return sym(ParserSym.PAR_DER); 	}
	")"						{ return sym(ParserSym.PAR_IZQ); 	}
	":"						{ return sym(ParserSym.DOS_PUNTOS); 	}
	","						{ return sym(ParserSym.COMA); 	}
		
	{entero}				{ return sym(ParserSym.NUM, new Integer(yytext())); 	}
	{id}					{ return sym(ParserSym.ID);  }
	
	//=================================== INICIO ESPACIOS EN BLANCO, COMENTARIOS Y ERRORES  =======================================	
	[" "\t]+				{ }
	.						{ compi.reportarErrorLexico(yytext(), yyline + 1, yycolumn); }	
}