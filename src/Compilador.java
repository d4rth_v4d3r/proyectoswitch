/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
public abstract class Compilador {
	private int temporales, etiquetas;
	public static final String heap_pointer = "h", stack_pointer = "p",
			heap = "heap", stack = "stack";

	public Compilador() {
		this.temporales = 1;
		this.etiquetas = 1;
	}

	/**
	 * Callback que se ejecuta al encontrar un error lexico.
	 * 
	 * @param token
	 *            El token no reconocido.
	 * @param linea
	 *            Linea del token.
	 * @param columna
	 *            Columna del token.
	 */
	public abstract void reportarErrorLexico(String token, int linea,
			int columna);

	/**
	 * Callback que se ejecuta al encontrar un error sintactico.
	 * 
	 * @param mensaje
	 *            El mensaje que describe el error.
	 * @param linea
	 *            Linea del token.
	 * @param columna
	 *            Columna del token.
	 */
	public abstract void reportarErrorSintactico(String mensaje, int linea,
			int columna);

	/**
	 * Callback que se ejecuta al encontrar un error semantico.
	 * 
	 * @param archivo
	 *            El nombre del archivo analizado.
	 * @param mensaje
	 *            El mensaje que describe el error de semántica.
	 * @param linea
	 *            La línea del token que causó el error.
	 * @param columna
	 *            La columna del token.
	 */
	public abstract void reportarErrorSemantico(String mensaje, int linea,
			int columna);

	/**
	 * Genera una variable temporal;
	 * 
	 * @return Un nuevo temporal;
	 */
	public String generaTMP() {
		return "t" + temporales++;
	}

	/**
	 * Genera una nueva etiqueta
	 * 
	 * @return La nueva etiqueta
	 */
	public String generaETQ() {
		return "L" + etiquetas++;
	}

	/**
	 * Instruccion para generar codigo.
	 * 
	 * @param codigo
	 *            La linea de codigo a escribir.
	 */
	public abstract void write(String codigo);
}
