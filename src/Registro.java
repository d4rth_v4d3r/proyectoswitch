public class Registro {
	public String etqV;
	public String etqF;
	public String var;

	public Registro() {

	}

	/**
	 * Crea una entrada. Si es necesario puede ampliarse
	 * 
	 * @param etqV
	 *            La etiqueta verdadera
	 * @param etqF
	 *            La etiqueta falsa
	 * @param var
	 *            El id.
	 */
	public Registro(String etqV, String etqF, String var) {
		// TODO Auto-generated constructor stub
		this.etqF = etqF;
		this.etqV = etqV;
		this.var = var;
	}

}
